class CarsController < ApplicationController
 
  def index
    @cars = Car.all
  end

  def new
	  	 @car = Car.new
    end

  def delete
  end

  def update
  end

  def show
  end


    def create
	    @car = Car.new(car_params)
	    if @car.save
	      flash[:success] = "Car added to database"
	      redirect_to root_url
	    else
	      render 'new'
	    end
  	end

  private

    def car_params
      params.require(:car).permit(:type, :lat, :lng)
    end

  
  
end
