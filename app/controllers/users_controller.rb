class UsersController < ApplicationController
  def new
  	 @user = User.new
  end

  def update
  end

  def delete
  end

  def charging
  end

  def show
    @user = User.find(params[:id])
  end
  
   def create
    @user = User.new(user_params)
    #user[:charg] = 1000
    if @user.save
      flash[:success] = "Welcome to the Sample App!"
      redirect_to @user
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email,:charg, :password,:password_confirmation)
    end

    def chargParams
      params.require(:userSharg).permit(:id,:name,:charg)
    end
  
end
