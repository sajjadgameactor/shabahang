class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :charg , :default => 1000
      t.boolean :isActive

      t.timestamps null: false
    end
  end
end
