class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :carType
      t.decimal :lat
      t.decimal :lng
      t.boolean :free

      t.timestamps null: false
    end
  end
end
