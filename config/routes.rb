Rails.application.routes.draw do
  
  get 'cars' => 'cars#index'

  get 'cars/new'

  get 'cars/delete'

  get 'cars/update'

  get 'cars/show'

  get 'sessions/new'

  get 'users/new'

  get 'users/update'

  get 'users/delete'

  root 'static_pages#home'

  get 'static_pages/home'

  get 'static_pages/about'

  get 'static_pages/policy'

  get 'charging' => 'users#charging'
  
  get 'about' => 'static_pages#about'

  get 'policy' => 'static_pages#policy'

  get 'signup'  => 'users#new'

  get    'login'   => 'sessions#new'
  
  post   'login'   => 'sessions#create'

  delete 'logout'  => 'sessions#destroy'
  
   resources :users

    
end
